PACKAGE=$(subst Package: ,,$(shell grep ^Package: DESCRIPTION))
VERSION=$(subst Version: ,,$(shell grep ^Version: DESCRIPTION))
VIGNETTES=$(wildcard vignettes/*.Rmd)
RFLAGS=--slave --no-restore
PKG_FILE=$(PACKAGE)_$(VERSION).tar.gz
R=R $(RFLAGS)
R_VERSIONS=4.1.2 4.2.3 latest

all:

build: NAMESPACE
	$(R) CMD build .

$(PKG_FILE): build

lint:
	$(R) -e "lintr::lint_package()"

check: $(PKG_FILE)
	$(R) CMD check --as-cran "$<"

NAMESPACE: doc

doc:
	$(R) -e "roxygen2::roxygenise()"

install: $(PKG_FILE)
	$(R) CMD INSTALL $(PKG_FILE)

public: check
	mkdir -p "$@"
	for f in fscache.Rcheck/fscache/doc/*.html ; do sed -e 's!<a href=".*/library/fscache/doc/!<a href="!' "$$f" >public/"$$(basename "$$f")" ; done

vignettes: $(VIGNETTES:.Rmd=.html)

%.html: %.Rmd
	R -e 'devtools::build_rmd("$<")'

test:
	$(R) -e "testthat::test_local()"

test_win: $(PKG_FILE)
	curl -T "$(PKG_FILE)" "ftp://win-builder.r-project.org/R-release/" --user "anonymous:pierrick.roger@cea.fr" # Current R release
	# /R-oldrelease/ for previous R release, and /R-devel/ for next R release

dockers: $(addprefix docker_,$(R_VERSIONS))

docker_%:
	docker buildx build --progress plain --build-arg "VERSION=$(patsubst docker_%,%,$@)" -t "r-fscache:$(patsubst docker_%,%,$@)" .

base_dockers: $(addprefix base_docker_,$(R_VERSIONS))

base_docker_%:
	docker buildx build --progress plain -f base_docker.dockerfile --build-arg "VERSION=$(patsubst base_docker_%,%,$@)" -t "registry.gitlab.com/cnrgh/databases/r-fscache:$(patsubst base_docker_%,%,$@)" .

push_base_dockers: $(addprefix push_base_docker_,$(R_VERSIONS))

push_base_docker_%:
	docker push "registry.gitlab.com/cnrgh/databases/r-fscache:$(patsubst push_base_docker_%,%,$@)"

# building on other platforms
archlinux rockylinux:
	docker buildx build --progress plain -f base_docker.dockerfile --build-arg "IMAGE=$@" -t "registry.gitlab.com/cnrgh/databases/r-fscache:$@" .
	docker buildx build --progress plain --build-arg "VERSION=$@" -t "r-fscache:$@" .

cov:
	./compute_coverage

clean:
	$(RM) *.tar.gz fs_state_*.txt
	$(RM) NAMESPACE vignettes/*.html
	$(RM) -r man *.Rcheck
	$(RM) tests/testthat/?.txt tests/testthat/?.tsv tests/testthat/?.csv
	$(RM) -r tests/testthat/wrk
	images="$$(docker images -q r-cache)" ; if test -n "$$images" ; then docker rmi $$images ; fi

.PHONY: all clean check doc install test vignettes lint
