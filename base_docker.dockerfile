# vi: ft=dockerfile
ARG VERSION=latest
ARG IMAGE=r-base
FROM ${IMAGE}:${VERSION}

# Debian
RUN if command -v apt-get ; then apt-get update && apt-get install -y qpdf tidy \
    texlive-base texlive-latex-base texinfo \
    texlive-fonts-recommended texlive-fonts-extra \
    texlive-latex-recommended libssl-dev \
    libcurl4-openssl-dev libxml2-dev pandoc ; fi

# Archlinux
RUN if command -v pacman ; then pacman --noconfirm -Syu make r texlive-bin gcc diffutils pandoc-cli qpdf tidy texinfo texlive-latexrecommended texlive-fontsextra texlive-fontsrecommended ; fi

# Install R packages
RUN R --slave --no-restore -e "install.packages(c('chk', 'covr', 'lgr', 'testthat', 'roxygen2', 'rstudioapi', 'R.utils', 'rmarkdown', 'lintr'), dependencies=TRUE, repos='https://cloud.r-project.org/')"
